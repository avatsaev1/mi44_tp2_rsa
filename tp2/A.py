
#-*-coding: utf-8 -*-

import random
import sys
import math
import C as c
import B as b

dico = {'A': 32,
		'B':33,
		'C':2,
		'D':3,
		'E':4,
		'F':5,
		'G':6,
		'H':7,
		'I':8,
		'J':9,
		'K':10,
		'L':11,
		'M':12,
		'N':13,
		'O':14,
		'P':15,
		'Q':16,
		'R':17,
		'S':18,
		'T':19,
		'U':20,
		'V':21,
		'W':22,
		'X':23,
		'Y':24,
		'Z':25,
		' ':26,
		'.':27,
		',':28,
		'\'':29,
		'!':30,
		'?':31}

dico_bin = {dico[k]:k for k in dico.keys()}

e = ""
d = ""
n = ""
rand = ""

def generate():

	global e
	global d
	global n

	cle = c.generate()
	e= cle[0]
	d = cle[1]
	n = cle[2]

def toInt(letter):
	return dico[letter]

def toLetter(num):
	return dico_bin[num]

def toASCII(num):
	return int(dico_bin[num])

def __fast_modular_exp(g,d,n):
	d = bin(d)
	# supprime les caractère "0b"
	d = d[2:]
	# reverse la chaine
	d = d[::-1]

	R0 = 1
	R1 = g
	for i in range(len(d)):
		if d[i]=="1":
			R0 = (R1*R0) % n
		R1 = (R1**2) % n
	return R0

def get_public_key():
	return e,n

def send_msg_to_B(msg):
	print "\n  > Message a encrypter : "+msg
	# Demande a B ça clé public
	cle_B = b.get_public_key()
	msg_crypter = []
	for caract in msg:
		msg_crypter.append(__fast_modular_exp(toInt(caract),int(cle_B[0]),int(cle_B[1])))

	print "\n  > Envoie du message crypter à B : "+str(msg_crypter)
	return msg_crypter

def decrypt_msg_from_B(msg_crypt):
	global d
	global n
	msg = ''
	for caract in msg_crypt:
		msg += toLetter(__fast_modular_exp(caract, d, n))

	print "\n  > Message décrypter par A : "+msg
	return msg


## Scenario de test

def etape_1():
	b.generate()
	generate()
	msg = "AB ?!"

	print "\n  --------------- 1 ------------------"

	msg_crypt = send_msg_to_B(msg)

	b.etape_1(msg_crypt)

def etape_2(msg_crypt):
	msg_decrypt = decrypt_msg_from_B(msg_crypt)
	etape_3(msg_decrypt)

def etape_3(msg_decrypt):
	global rand

	print "\n  ------------------- 3 -------------------"
	if msg_decrypt == "AB OK":
		rand = ''
		for _ in range(4):
			rand += toLetter(random.randint(2,33))
		print "\n  > 4 caractères aléatoire : "+rand

		msg_crypt = send_msg_to_B(rand)
		b.etape_3(msg_crypt)

def etape_3_suite(msg_crypt):
	msg_decrypt = decrypt_msg_from_B(msg_crypt)
	etape_4(msg_decrypt)

def etape_4(msg_decrypt):
	print "\n  ------------------- 4 -------------------"
	if msg_decrypt == rand:
		print "OK"


def main():
	msg_crypt = etape_1()


# excecute seulement si on lance A.py
if __name__ == '__main__':
	b.main()
